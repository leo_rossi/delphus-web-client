import styled from 'styled-components';
import { Col, Row } from 'react-bootstrap';

export const Content = styled('div') `
    z-index: 0;
    margin: 10% 5%;
    color: #2A2F4D;
`

export const ContentTitle = styled(Row) `
    justify-content: center;
    margin-bottom: 1.5rem;
`

export const Article = styled(Col) `
    text-align: center;
    padding: 1rem 0;
`

export const Icon = styled('i') `
    margin-bottom: 1rem;
`

export const Paragraph = styled('p') `
    text-align: justify;
`