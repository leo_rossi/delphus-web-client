import React, { Component } from 'react'
import { Row } from 'react-bootstrap';
import { Article, Content, ContentTitle, Icon, Paragraph } from './styles';

export default class About extends Component {
    render() {
        return (
            <Content id="#about">
                <ContentTitle>
                    <h1>About Delphus</h1>
                </ContentTitle>
                <Row>
                    <Article md={4}>
                        <Icon className="fa fa-chart-line fa-5x"></Icon>
                        <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae nisi mollis, suscipit libero vel, luctus metus. Praesent nisl mauris, placerat sed consequat non, pellentesque semper lectus. Integer ut nulla sed neque aliquet ultricies nec a purus. Curabitur ac metus odio. Pellentesque quis consequat eros. Donec ac magna id urna consectetur molestie. Mauris auctor orci et arcu auctor rutrum. Donec dapibus condimentum sem sed elementum. Vestibulum suscipit ligula massa, non porta lectus auctor id. Sed nec risus vel elit suscipit tincidunt vitae sit amet nibh. Vivamus id elit eget ex sodales mollis. Phasellus posuere a tellus at luctus.</Paragraph>
                    </Article>
                    <Article md={4}>
                        <Icon className="fab fa-github fa-5x"></Icon>
                        <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae nisi mollis, suscipit libero vel, luctus metus. Praesent nisl mauris, placerat sed consequat non, pellentesque semper lectus. Integer ut nulla sed neque aliquet ultricies nec a purus. Curabitur ac metus odio. Pellentesque quis consequat eros. Donec ac magna id urna consectetur molestie. Mauris auctor orci et arcu auctor rutrum. Donec dapibus condimentum sem sed elementum. Vestibulum suscipit ligula massa, non porta lectus auctor id. Sed nec risus vel elit suscipit tincidunt vitae sit amet nibh. Vivamus id elit eget ex sodales mollis. Phasellus posuere a tellus at luctus.</Paragraph>
                    </Article>
                    <Article md={4}>
                        <Icon className="fa fa-users fa-5x"></Icon>
                        <Paragraph>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae nisi mollis, suscipit libero vel, luctus metus. Praesent nisl mauris, placerat sed consequat non, pellentesque semper lectus. Integer ut nulla sed neque aliquet ultricies nec a purus. Curabitur ac metus odio. Pellentesque quis consequat eros. Donec ac magna id urna consectetur molestie. Mauris auctor orci et arcu auctor rutrum. Donec dapibus condimentum sem sed elementum. Vestibulum suscipit ligula massa, non porta lectus auctor id. Sed nec risus vel elit suscipit tincidunt vitae sit amet nibh. Vivamus id elit eget ex sodales mollis. Phasellus posuere a tellus at luctus.</Paragraph>
                    </Article>
                </Row>
            </Content>
        )
    }
}
