import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux' 
import * as RoutingActions from '../../store/actions/routingActions'
import { findReposByUsername } from '../../resources/githubResource'

import { Link } from 'react-router-dom'
import * as ChartStyles from './styles'
import PieChart from '../../components/pieChart'
import LangChart from '../../components/langChart'

class Charts extends Component {

    constructor(props) {
        super(props);

        this.state = {
            repos: [],
            langs: false,
            error: false
        }
    }

    componentDidMount() {
        this.props.fixNavbar(true);
        findReposByUsername(this.props.match.params.username)
            .then(res => {
                console.log(res.data)
                this.setState({repos: res.data})
                this.setState({error: !this.state.repos || this.state.repos.length === 0})
            })
            .catch(err => this.setState({error: true}));
    }

    render() {
        return this.state.error ? 
                (
                    <ChartStyles.ErrorContainer>
                        <ChartStyles.JustifiedRow justify={'center'}>
                            <ChartStyles.ErrorCol md={12}>
                                <h2>Somethings are only for the gods to know...</h2>
                                <p>We actually couldn't find anything about <strong>{this.props.match.params.username}</strong></p>
                                <p>Make sure the username you typed is correct and that the user has already one public repo</p>
                                <p><Link to="/">Back to home</Link></p>
                            </ChartStyles.ErrorCol>
                        </ChartStyles.JustifiedRow>
                    </ChartStyles.ErrorContainer>
                )
                :
                (
                    <ChartStyles.Container>
                        <ChartStyles.JustifiedRow justify={'center'}>
                            <ChartStyles.CustomCard width={95}>
                                <ChartStyles.CustomCard.Title>
                                    <ChartStyles.Title>The oracle answers...</ChartStyles.Title>
                                </ChartStyles.CustomCard.Title>
                                <ChartStyles.CustomCard.Subtitle>
                                    <ChartStyles.Subtitle>Here's what we found about <strong>{this.props.match.params.username}</strong></ChartStyles.Subtitle>
                                </ChartStyles.CustomCard.Subtitle>
                            </ChartStyles.CustomCard>
                        </ChartStyles.JustifiedRow>
        
                        <ChartStyles.JustifiedRow justify={'center'}>
                            <ChartStyles.CustomCard width={95}>
                                <ChartStyles.ControlRow>
                                    <ChartStyles.SelectedControlTitle>{this.state.langs ? 'Languages' : 'Projects' }</ChartStyles.SelectedControlTitle>
                                    <ChartStyles.Switch type='radio' name="langs">
                                        <ChartStyles.Toggle variant={'outline-primary'} onClick={ev => this.setState({ langs: false })} selected={!this.state.langs}>Projects</ChartStyles.Toggle>
                                        <ChartStyles.Toggle variant={'outline-primary'} onClick={ev => this.setState({ langs: true })} selected={this.state.langs}>Languages</ChartStyles.Toggle>
                                    </ChartStyles.Switch>
                                </ChartStyles.ControlRow>
        
                                <ChartStyles.JustifiedRow justify={'space-around'}>
                                    {
                                        this.state.langs ? 
                                            (
                                                <LangChart username={this.props.match.params.username} />
                                            ) :
                                            this.state.repos.map((repo, index) => (
                                                <PieChart key={index} width={150} height={150}
                                                        username={this.props.match.params.username} 
                                                        reponame={repo.name} />
                                            ))
                                    }
                                    
                                </ChartStyles.JustifiedRow>
                            </ChartStyles.CustomCard>
                        </ChartStyles.JustifiedRow>                
                    </ChartStyles.Container>
                )
            
    }
}

const mapStateToProps = state => ({

});

const mapDispatchToProps = dispatch => bindActionCreators(RoutingActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Charts);
