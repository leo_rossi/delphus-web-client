import styled from 'styled-components'
import { Col, Row, Card, ToggleButtonGroup, ToggleButton } from 'react-bootstrap'
import Colors from '../../constants/colors'

export const Container = styled('div') `
    padding-top: 4rem;
`

export const ErrorContainer = styled('div') `
    margin-top: 20%;

    h2, p {
        color: ${ Colors[900] } !important
        display: block;
        justify-content: center;
        text-align: center;
    }

    h2 {
        margin-bottom: 1rem;
    }

    p {
        margin-bottom: 0.5rem;
    }

    a {
        color: ${ Colors[900] };
        text-decoration: underline;

        &:hover {
            color: ${ Colors[600] }
        }
    }
`

export const ErrorCol = styled(Col) `
    justify-content: center;
`

export const CustomCard = styled(Card) `
    margin: 1rem 0;
    width: ${props => props.width }%;
    border: none !important;
    box-shadow: 1px 1.5px 5px gray !important;
    padding-top: 1rem;
    padding-left: 1rem;
`

export const JustifiedRow = styled(Row) `
    justify-content: ${ props => props.justify };
`

export const Title = styled('h2') `
    color: ${Colors[900]} !important;
`

export const Subtitle = styled('p') `
    color: ${ Colors[900] } !important
`

export const ControlRow = styled(Row) `
    justify-content: space-between;
    padding-bottom: 1rem;
    padding-right: 3rem;
    vertical-align: middle !important;
    z-index: 2;
`

export const Toggle = styled(ToggleButton) `
    border-color: ${ Colors[900] } !important;
    color: ${ props => props.selected ? 'white' : Colors[900] } !important;
    background-color: ${ props => props.selected ? Colors[700] : 'white' } !important;

    &:hover {
        background-color: ${ Colors[500] } !important;
        color: ${ Colors.contrast } !important;
        cursor: pointer;
    }

    &:focus {
        box-shadow: none !important;
        transition: none;
    }
`

export const SelectedControlTitle = styled('h5') `
    margin-left: 3rem;
    margin-top: 0.4rem;
    margin-bottom: 0;
    color: ${ Colors[900] } !important;
`

export const Switch = styled(ToggleButtonGroup) `
    margin-right: 1rem;
    vertical-align: middle !important;
`

export const Menu = styled(Col) `
    justify-content: right;
    text-align: right;
    padding-right: 1rem;
    border-right: 1px groove rgba(51, 57, 73, .3);
`

export const CardCols = styled(Col) `
    margin-left: 60%;
`