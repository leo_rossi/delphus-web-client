import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { BackgroundColor, BackgroundImage, Content, Subtext } from './styles'
import SearchBar from '../../components/searchBar'
import cyprien from '../../assets/cosmic-timetraveler-te2hE5S9d78-unsplash.jpg'
import About from '../about';
import Footer from '../../components/footer'

import * as RoutingActions from '../../store/actions/routingActions'


class Home extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: ''
        }
    }

    componentWillMount() {
        this.props.fixNavbar(false);
    }

    render() {
        return (
            <div>
                <BackgroundColor id="home">
                    <BackgroundImage src={cyprien}></BackgroundImage>
                    <Content>
                        <h1>Know thyself</h1>
                        <Subtext>Get started by typing your Github username below</Subtext>
                        <SearchBar />
                    </Content>
                </BackgroundColor>
                <About></About>
                <Footer></Footer>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators(RoutingActions, dispatch);

export default connect(state => ({}), mapDispatchToProps)(Home);