import styled from 'styled-components'

export const BackgroundColor = styled('div') `
    background-color: #555B7D;
    left: 0;
    top: 0;
    padding-top: -40px;
    margin-top: 0;
`

export const BackgroundImage = styled('img') `
    width: 100%;
    height: 100%;
    left: 0;
    top: 0;
    margin-top: -200px;
    z-index: -1;
    opacity: .7;
`

export const Content = styled('div') `
    position: absolute;
    z-index: 0;
    margin: 0 auto;
    left: 0;
    right: 0;
    top: 40%; /* Adjust this value to move the positioned div up and down */
    text-align: center;
    color: white
`

export const Subtext = styled('p') `
    font-size: large;
`