import axios from 'axios'

export function findReposByUsername(username) {
    return axios.get('/users/' + username + '/repos');
}

export function findLanguagesByRepoName(username, repoName) {
    return axios.get('/repos/' + username + '/' + repoName + '/languages');
}