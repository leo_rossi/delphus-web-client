import axios from 'axios'
import { dispatch } from 'redux'
import * as LoadingActions from '../store/actions/loadingActions'

axios.defaults.baseURL = 'http://api.github.com'
axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';
axios.defaults.headers.common['Access-Control-Allow-Methods'] = 'GET, POST, OPTIONS, PUT, PATCH, DELETE'
axios.defaults.headers.common['Accept'] = 'application/vnd.github.inertia-preview+json'

axios.interceptors.request.use(config => {
    dispatch(LoadingActions.toggleLoading(true));
    return config;
}, error => Promise.reject(error));

axios.interceptors.response.use(config => {
    dispatch(LoadingActions.toggleLoading(false));
    return config;
}, error => {
    dispatch(LoadingActions.toggleLoading(false));
    Promise.reject(error);
});