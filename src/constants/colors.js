export const Colors = {
    '50': '#ebebef',
    '100': '#ccced8',
    '200': '#aaadbe',
    '300': '#888ca4',
    '400': '#6f7491',
    '500': '#555b7d',
    '600': '#4e5375',
    '700': '#44496a',
    '800': '#3b4060',
    '900': '#2a2f4d',
    'A100': '#98a5ff',
    'A200': '#6578ff',
    'A400': '#324bff',
    'A700': '#1935ff',
    'contrast': '#fff'
}

export default Colors;