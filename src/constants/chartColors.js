const ChartColors = [
    '#6f7491',
    '#98a5ff',
    '#2a2f4d',
    '#ebebef',
    '#3b4060',
    '#1935ff',
    '#ccced8',
    '#324bff',
    '#aaadbe',
    '#6578ff',
    '#888ca4',
    '#555b7d',
    '#44496a',
    '#4e5375',
]

export default ChartColors;