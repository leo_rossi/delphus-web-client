export function toggleLoading(isLoading) {
    return {
        type: 'TOGGLE_LOADING',
        isLoading
    }
}