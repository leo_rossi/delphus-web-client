export function fixNavbar(fixed) {
    return {
        type: 'FIX_NAVBAR',
        fixed
    }
}