const INITIAL_STATE = {
    fixedNav: false
}

export default function routing(state = INITIAL_STATE, action) {
    if (action.type === 'FIX_NAVBAR') {
        state = {
            ...state,
            fixedNav: action.fixed
        }
    }
    return state;
}