const INITIAL_STATE = {
    isLoading: false
}

export default function loading(state = INITIAL_STATE, action) {
    if (action.type === 'TOGGLE_LOADING') {
        state = {
            ...state,
            isLoading: action.isLoading
        }
        // console.log(state)
    }
    return state;
}