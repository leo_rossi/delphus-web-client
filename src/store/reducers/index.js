import { combineReducers } from 'redux';

import loading from './loadingReducer';
import routing from './routingReducer';

export default combineReducers({
    loading,
    routing
});