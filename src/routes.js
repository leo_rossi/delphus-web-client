import React, { Component } from 'react'
import { Route, Switch } from 'react-router-dom';

import Home from './pages/home';
import Charts from './pages/charts'

const routes = [
    {
        path: '/',
        component: Home,
        exact: true
    },
    {
        path: '/user/:username',
        component: Charts,
        exact: false
    }
]

export default class Routes extends Component {
    render() {
        return (
            <Switch>
                { routes.map((route, idx) => 
                    <Route key={ idx } path={ route.path } exact={ route.exact } component={ route.component } />
                )}
            </Switch>
        )
    }
}