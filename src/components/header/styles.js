import styled from 'styled-components';
import { Button, Navbar } from 'react-bootstrap';
import Colors from '../../constants/colors'

export const NavMenu = styled(Navbar) `
    position: fixed !important;
    width: 100%
    z-index: 5;
    left: 0;
    top: 0;
    box-shadow: ${ props => props.scrollpos > 200 || props.fixed ? '1px 1px 5px gray' : 'none'} !important;
    background-color: ${ props => 
            props.fixed  ? 'rgba(255, 255, 255, 1)' 
                         : props.scrollpos > 1 ?  calcColor(props.scrollpos) 
                                               : 'transparent' 
    } !important

    * {
        color: ${ props => props.scrollpos > 1 || props.fixed ? Colors[900] : Colors.contrast } !important
    }

    a:hover {
        text-decoration: none;
    }
`

export const LoginButton = styled(Button) `
    color: #555B7D !important;
    margin: 0px 5px !important;
`

export const RegisterButton = styled(Button) `
    border-color: ${props => props.scrollpos > 1 || props.fixed ? '#656B8D' : '#fff'} !important;

    &:hover {
        color: #555B7D !important;
    }
`

const calcColor = y => {
    return `rgba(255, 255, 255, ${.005 * y})`
}