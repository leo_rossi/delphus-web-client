import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

import { Nav } from 'react-bootstrap'
import { NavMenu, LoginButton, RegisterButton } from './styles'


class Header extends Component {

    state = {
        scrollPos: 0
    }

    constructor(props) {
        super(props);
        this.handleScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll);
    }

    handleScroll = e => {
        let y = 1 + (window.scrollY || window.pageYOffset);
        y = y < 1 ? 1 : y // ensure y is always >= 1 (due to Safari's elastic scroll)
        this.setState({'scrollPos': y});

    }

    render() {
        return (
            <NavMenu expand="lg" scrollpos={this.state.scrollPos} fixed={this.props.fixed} id="header">
                <NavMenu.Brand><Link to='/' >Delphus</Link></NavMenu.Brand>
                <NavMenu.Toggle aria-controls="basic-navbar-nav" />
                <NavMenu.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                    </Nav>
                    <RegisterButton variant="outline-light" scrollpos={this.state.scrollPos} fixed={this.props.fixed}>Sign up</RegisterButton>
                    <LoginButton variant="light">Sign in</LoginButton>
                </NavMenu.Collapse>
            </NavMenu>
        )
    }
}

const mapStateToProps = state => ({
    fixed: state.routing.fixedNav
});

export default connect(mapStateToProps)(Header);
