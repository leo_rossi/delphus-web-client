import styled from 'styled-components'
import { Row } from 'react-bootstrap'
import Colors from '../../constants/colors';

export const Container = styled('div') `
    background-color: ${ Colors[700] };
    padding-top: 1.5rem;
    padding-bottom: 1.5rem;
    
    * {
        color: ${ Colors.contrast };
    }

    a:hover {
        color: ${ Colors.contrast };
    }
`

export const JustifiedRow = styled(Row) `
    justify-content: center;
    
    * {
        justify-content: center;
    }

    p:last-child {
        margin-bottom: 0;
    }
`