import React from 'react'
import * as FooterStyles from './styles'

const Footer = () => {
    return (
        <FooterStyles.Container>
            <FooterStyles.JustifiedRow>
                <p>Credits on the photo to Cosmic Timetraveler from <a href="https://unsplash.com/@cosmictimetraveler">Unsplash</a></p>
            </FooterStyles.JustifiedRow>
        </FooterStyles.Container>
    )
}

export default Footer;
