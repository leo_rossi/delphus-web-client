import React, { Component } from 'react'
import { findReposByUsername, findLanguagesByRepoName } from '../../resources/githubResource';
import { VictoryPie, VictoryTooltip } from 'victory';
import ChartColors from '../../constants/chartColors'
import * as LangChartStyles from './styles'
import { Row, Col } from 'react-bootstrap';

export default class LangChart extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            loc: 0,
            projects: 0,
            numOfLangs: 0,
            errors: false
        }
    }

    componentDidMount() {
        const { username } = this.props;
        const langs = {};
        findReposByUsername(username)
            .then(res => {
                this.setState({projects: res.data.length});
                console.log(this.state.projects)
                res.data.forEach(repo => {
                    findLanguagesByRepoName(username, repo.name)
                        .then(res => {
                            Object.keys(res.data).forEach(projectLang => {
                                if (Object.keys(langs).includes(projectLang)) {
                                    langs[projectLang] += res.data[projectLang];
                                } else {
                                    langs[projectLang] = res.data[projectLang];
                                }
                            })
                            this.setState({ loc: Object.values(langs).reduce((a, b) => a + b) });
                            this.setState({data: Object.keys(langs).map(lang => ({x: lang, y: langs[lang], label: lang + ' (' + parseFloat(langs[lang] * 100 / this.state.loc).toFixed(2) + '%)'}))});
                            this.setState({numOfLangs: this.state.data.length });
                        })
                        .catch(err => this.setState({ errors: true }));
                });
            })
            .catch(err => this.setState({ errors: true }));
    }

    render() {
        return (
            <LangChartStyles.ChartCard>
                <LangChartStyles.ChartCard.Body>
                    <LangChartStyles.ChartCard.Title>{this.props.username}</LangChartStyles.ChartCard.Title>
                    { this.state.data && this.state.data.length > 0 ?                     
                        ( 
                        <Row>
                            <Col md={3} style={{marginTop: '4.5rem'}}>
                                <p><strong>Projects</strong>: {this.state.projects}</p>
                                <p><strong>Lines of Code</strong>: {this.state.loc}</p>
                                <p><strong>Language Diversity</strong>: {this.state.numOfLangs}</p>
                            </Col>
                            <Col md={9}>
                                {/* <VictoryChart polar> */}
                                    <VictoryPie data={this.state.data}
                                        width={200} height={200}
                                        animate={{easing: 'exp', duration: 500}}
                                        colorScale={ChartColors}
                                        labels={({datum}) => datum.x }
                                        style={{ labels: { fill: "white"} }}
                                        labelComponent={
                                            <VictoryTooltip flyoutStyle={{fill: '#2A2F4D', stroke: 'none'}}
                                                style={{fontSize: '5px', fill: 'white', verticalAlign: 'middle', fontFamily: 'Lato'}}
                                                constrainToVisibleArea={true} />
                                        }
                                    /> 
                                {/* </VictoryChart> */}
                            </Col>
                        </Row>
                        ) :

                        (
                            <LangChartStyles.ChartPlaceholder>
                                <h5>No data found</h5>
                                <p>This project may be empty or <br/>contain only not code-related files</p>
                            </LangChartStyles.ChartPlaceholder>
                        )
                    }
                </LangChartStyles.ChartCard.Body>
            </LangChartStyles.ChartCard>
        )
    }
}
