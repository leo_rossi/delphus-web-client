import styled from 'styled-components'
import ReactLoading from 'react-loading';

export const progressStyles = {
    barColors: {
        '0': '#2A2F4D',
        '0.5': '#555B7D',
        '1.0': '#fff'
    },
    barThickness: 5,
    shadowBlur: 0
};

export const LoadingContainer = styled('div') `
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    overflow-x: none;
    overflow-y: none;
    background-color: rgba(255, 255, 255, .8);
    justify-content: center;
    text-align: center;
    z-index: 2;
    animation: ${props => props.fadeOut ? 'fadeOut .5s linear' : 'none'};
    animation-fill-mode: forwards;

    * {
        color: #2A2F4D;
    }

    @keyframes fadeOut {
        0% { 
            opacity: 1;
            display: auto;
            z-index: 2;
        }
        100% { 
            opacity: 0; 
            display: none;
            z-index: -1;
        }
    }
`

export const Logo = styled('h1') `
    margin-top: 20%;
`

export const Spin = styled(ReactLoading) `
    margin-top: 20%;
    margin-left: 48%;
`

