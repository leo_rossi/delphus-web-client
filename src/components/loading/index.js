import React from 'react'
import TopBarProgress from 'react-topbar-progress-indicator'
import { connect } from 'react-redux';

import { progressStyles, LoadingContainer, Spin } from './styles'


TopBarProgress.config(progressStyles);
const Loading = ({ isLoading }) => (
            <LoadingContainer fadeOut={!isLoading}>
                { isLoading ? (<TopBarProgress />) : '' }
                <Spin type={"spin"} color={'#2A2F4D'} />
            </LoadingContainer>
)

const mapStateToProps = state => ({
    isLoading: state.loading.isLoading
});

export default connect(mapStateToProps)(Loading);