import styled from 'styled-components'
import { Card } from 'react-bootstrap';
import Colors from '../../constants/colors'

export const ChartCard = styled(Card) `
    margin: 1rem;
    width: 20%,
    height: 20%;
    box-shadow: 1px 1.5px 3px 0 rgba(0,0,0,.2) !important;
    border: none !important;
    border-radius: 0.2rem;

    &:hover {
        box-shadow: 1px 1.5px 5px gray !important;
    }

    * {
        color: ${ Colors[900] } !important;
    }
`

export const ChartPlaceholder = styled(ChartCard.Body) `
    padding-top: 25% !important;
`