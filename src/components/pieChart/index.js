import React from 'react'
import { VictoryPie, VictoryTooltip, VictoryTheme } from 'victory'
import { findLanguagesByRepoName } from '../../resources/githubResource';
import * as PieChartStyles from './styles';
import ChartColors from '../../constants/chartColors'


export default class PieChart extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            data: [{y: 100}],
            loc: 0
        }
    }

    componentDidMount() {
        const { username, reponame } = this.props;
        findLanguagesByRepoName(username, reponame)
            .then(res => {
                let langs = [];
                Object.keys(res.data).forEach(lang => {
                    langs.push({x: lang, y: res.data[lang], label: lang + ' (' + res.data[lang] + ' lines)'});
                });
                this.setState({data: langs});
                this.setState({loc: this.state.data.map(lang => lang.y).reduce((a, b) => a + b)});
            })
            .catch(err => console.log(err));
    }


    render() {
        return (
            <PieChartStyles.ChartCard>
                <PieChartStyles.ChartCard.Body>
                    <PieChartStyles.ChartCard.Title>{this.props.reponame}</PieChartStyles.ChartCard.Title>
                    { this.state.data && this.state.data.length > 0 ?                     
                        ( <VictoryPie data={this.state.data}
                            width={250} height={250}
                            animate={{easing: 'exp', duration: 500}}
                            colorScale={ChartColors}
                            theme={VictoryTheme.material}
                            labels={({datum}) => datum.y}
                            style={{ labels: { fill: "white" } }}
                            labelComponent={
                                <VictoryTooltip flyoutStyle={{fill: '#2A2F4D', stroke: 'none'}} 
                                constrainToVisibleArea={true} />
                            }
                        /> ) :

                        (
                            <PieChartStyles.ChartPlaceholder>
                                <h5>Only the gods know...</h5>
                                <p>This project may be empty or <br/>contain only not code-related files</p>
                            </PieChartStyles.ChartPlaceholder>
                        )
                    }
                </PieChartStyles.ChartCard.Body>
            </PieChartStyles.ChartCard>
        );
    }
}