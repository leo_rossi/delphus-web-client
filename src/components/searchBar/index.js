import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'

import { Row } from 'react-bootstrap'
import * as SearchBarStyles from './styles'

const SearchBar = () => {
    const [username, setUsername] = useState('');
    const [errors, setErrors] = useState(false);

    const history = useHistory();

    const handleChange = event => {
        setUsername(event.target.value);
        setErrors(!username);   
        console.log(errors)
    }

    const handleClick = event => {
        if (username && !errors) {
            history.push('/user/' + username);
        }
        setErrors(!username);
        console.log(errors)
    }
    
    return (
        <div>
            <Row>
                <SearchBarStyles.Search 
                        type="text"
                        error={errors.toString()}
                        placeholder={ errors ? 'Please fill this field' : 'Github username...'}
                        onChange={ event => handleChange(event) }/>
                <SearchBarStyles.SearchButton variant={"light"} onKeyDown={event => handleClick(event) }>
                    <i className="fa fa-search"></i>
                </SearchBarStyles.SearchButton>
            </Row>
        </div>
    );
}

export default SearchBar;