import styled from 'styled-components'
import { Button, FormControl, Row } from 'react-bootstrap'

export const Search = styled(FormControl) `
    border-radius: 1rem !important;
    width: 30% !important;
    border: '1px solid white !important';
    margin-left: 35%;

    &:focus {
        box-shadow: 0 0 0 0.1rem #555B7D !important;
    }
`
export const SearchButton = styled(Button) `
    border-radius: 50% !important;
    margin-left: 0.5rem;
`

export const ErrorContainer = styled(Row) `
    justify-content: center;

    * {
        color: #e0ad1f !important
    }
`