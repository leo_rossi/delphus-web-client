import React from 'react';
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom';

import store from './store'

import { Style } from './styles'

import Header from './components/header/index'
import Loading from './components/loading';
import Routes from './routes'

function App() {
  return (
    <div>
      <Provider store={store}>
        <BrowserRouter>
          <Loading />
          <Style/>
          <Header/>
          <Routes />
          {/* <Home /> */}
          {/* <About /> */}
        </BrowserRouter>
      </Provider>
    </div>
  );
}

export default App;
