import { createGlobalStyle } from 'styled-components'

export const Style = createGlobalStyle `
    html {
        overflow-x: hidden;
        body {
            @import url('https://fonts.googleapis.com/css?family=Lato|Montserrat&display=swap');
            font-family: 'Lato', serif;
        }
    }

`